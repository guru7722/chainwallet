import React, { useEffect } from "react";
import "../Style/Home.css";
import CarderHolder from "../comps/Cardholder/CarderHolder";

function Home() {
  useEffect(() => {
    const encryptedphrase = localStorage.getItem("phrase");
    if (encryptedphrase != null) {
      window.location = "/dashboard";
    }
  });
  return (
    <div className="Home">
      <h1>New to Chain Wallet?</h1>
      <CarderHolder />
    </div>
  );
}

export default Home;
