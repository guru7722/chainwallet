import React, { useEffect, useContext } from "react";
import CryptoJS from "crypto-js";
import "../Style/login.css";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { ethers } from "ethers";
import { TiTick } from "react-icons/ti";
import { ImCross } from "react-icons/im";
import { passcontext } from "../App"; //context from app js
function Login({ passfor }) {
  const { logout } = useContext(passcontext);
  const navigate = useNavigate();
  const [isDone, setdone] = useState(false);
  const [isclicked, setsclicked] = useState(false);
  const [passMatched, setmatch] = useState(false);
  const [password, setpass] = useState("");
  const [islogut, setlogout] = logout;
  useEffect(() => {
    const cookies = document.cookie;
    if (cookies != null) {
      navigate("/dashboard");
    }
  });
  useEffect(() => {
    const pass = localStorage.getItem("pass");
    var bytes = CryptoJS.AES.decrypt(pass, "123 my secret key");
    const decrypted = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    setpass(decrypted.toString());
  });
  useEffect(() => {
    console.log("matched");
  }, [setmatch]);
  // when log out delete cookie
  useEffect(() => {
    if (islogut) {
      deleteAllCookies();
    }
  }, [islogut]);

  //deleteCookies
  const deleteAllCookies = () => {
    const cookies = document.cookie.split(";");
    for (const cookie of cookies) {
      const eqPos = cookie.indexOf("=");
      const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  };

  //--------------------------------
  useEffect(() => {
    console.log("passRender");
  }, [passMatched]);

  async function CreateWallet() {
    try {
      setsclicked(true);
      setdone(true);
      document.cookie =
        "username=John Smith; expires=Thu, 18 Dec 2099 12:00:00 UTC; path=/";
      navigate("/dashboard");
    } catch (error) {
      console.log("password wrong", error);
    }
  }

  return (
    <div className="login-card">
      <p> login</p>
      <div className="inp-holder">
        <input
          type="password"
          placeholder="password"
          onChange={(e) => {
            if (e.target.value === password && e.target.value.length > 0) {
              setmatch(true);
            } else {
              console.log("wrong password");
              setmatch(false);
            }
          }}
        />
        {passMatched ? (
          <TiTick className="pass-icon" />
        ) : (
          <ImCross className="pass-icon" />
        )}
      </div>
      {isclicked && passMatched ? (
        isDone ? (
          <p className="wait-task">Done !</p>
        ) : (
          <p className="wait-task">wait sec .....</p>
        )
      ) : (
        <></>
      )}

      {passMatched ? (
        <button onClick={CreateWallet} className="btn-enabled">
          login
        </button>
      ) : (
        <button className="btn-disabled">login</button>
      )}
    </div>
  );
}

export default Login;
