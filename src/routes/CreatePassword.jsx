import React, { useEffect, useState } from "react";
import "../Style/CreatePassword.css";
import { ethers } from "ethers";
import { useNavigate } from "react-router-dom";
import CryptoJS from "crypto-js";

function CreateWallet() {
  const [Password, setpassword] = useState("");
  const [isMatched, setmatch] = useState(true);
  const [created, setcreated] = useState(false);
  const [isclicked, setclicked] = useState(false);
  const navigate = useNavigate();
  const [errormsg, setlengtherror] = useState(false);
  const [defaultAccountName, setaccountName] = useState(["Account0"]);
  useEffect(() => {
    if (created) {
      const encryptedphrase = localStorage.getItem("phrase");
      if (encryptedphrase != null) {
        if (isclicked) {
          navigate("/dashboard");
        }
      }
    }
  }, [created]);

  useEffect(() => {
    localStorage.setItem("SelectedWallet", 0); //on create default selected wallet
    localStorage.setItem("nameList", JSON.stringify([defaultAccountName])); //default account name
    localStorage.setItem("network", "rinkeby"); //default network
    const encryptedphrase = localStorage.getItem("phrase");
    if (encryptedphrase != null) {
      navigate("/dashboard");
    }
  });

  // Creating wallet and encrypting it and storing it on local Storage
  function CreateWallet() {
    setclicked(true); //when create button is clicked
    console.log("button working fine");
    const wallet = ethers.Wallet.createRandom(); //Creating Wallet Instanse
    const Phrasehere = wallet.mnemonic.phrase;
    encryptphraseAndStroeItinLocalStorage(Phrasehere);
    // encrypt(wallet);
    const encryptPasswordAndstorItinLocalStorage = CryptoJS.AES.encrypt(
      Password,
      "123 my secret key"
    );
    //storing it in local storage
    localStorage.setItem("pass", encryptPasswordAndstorItinLocalStorage);
    setcreated(true);
  }

  //encrypting phrases
  function encryptphraseAndStroeItinLocalStorage(mnemonic) {
    const encrytpingPhrase = CryptoJS.AES.encrypt(
      JSON.stringify({ mnemonic }),
      "123 my secret key"
    );
    localStorage.setItem("phrase", encrytpingPhrase);
  }

  return (
    <div className="Create-Wallet">
      <h1>Create Password</h1>
      {console.log("render")}
      <input
        type={"password"}
        placeholder={"Password"}
        value={Password}
        minLength={8}
        onChange={(e) => {
          e.preventDefault();
          setpassword(e.target.value);
          if (e.target.value.length < 8) {
            setlengtherror(true);
          } else {
            setlengtherror(false);
          }
        }}
      />
      {errormsg && <p>Password not long enough</p>}
      <input
        type={"password"}
        minLength={8}
        placeholder={"Confirm-Password"}
        onChange={(e) => {
          e.preventDefault();
          if (e.target.value === Password) {
            setmatch(true);
          } else {
            setmatch(false);
          }
        }}
      />
      {isclicked ? (
        created ? (
          <>Done!</>
        ) : (
          <>wait wallet encrypting .....</>
        )
      ) : (
        <></>
      )}
      {isMatched ? <></> : <p>Password does not matched</p>}
      {}
      {isMatched ? (
        errormsg ? (
          <button>Create</button>
        ) : (
          <button onClick={CreateWallet}>Create</button>
        )
      ) : (
        <button>Create</button>
      )}
    </div>
  );
}

export default CreateWallet;
