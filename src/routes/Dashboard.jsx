import React, { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { passcontext } from "../App";
import { ethers } from "ethers";
import CryptoJS from "crypto-js";
import "../Style/dashboard.css";

//icons
import { BsThreeDotsVertical } from "react-icons/bs";
import { MdContentCopy } from "react-icons/md";
import { RiExternalLinkLine } from "react-icons/ri";
import { TbApps } from "react-icons/tb";
import { FaEthereum } from "react-icons/fa";
import { GrFormClose } from "react-icons/gr";
import { BsKey } from "react-icons/bs";
import { IoLogOutOutline } from "react-icons/io5";
import { BsArrowUpRight } from "react-icons/bs";
import TransactionCard from "../comps/transactionCard/TransactionCard";

function Dashboard({
  setlogin,
  rendered,
  ShowCreateAccountCard,
  setcreateAccountCard,
  setcreated,
  accountCreated,
}) {
  const [empty, setempty] = useState(false);
  const [sideIcon, setside] = useState(false);
  const [provider, setprovider] = useState(null);
  const { logout } = useContext(passcontext);
  const [islogut, setlogout] = logout;
  const [WalletAddress, setaddress] = useState("");
  const [walletPrivateKey, setPrivatekey] = useState("");
  const [totalETH, setETH] = useState("");
  const [showNetwork, setShowNetwork] = useState("");
  const [showPrivateKey, setshowkey] = useState(false);
  const [newAccountName, setname] = useState("");
  const [myphrase, setphrases] = useState("");
  const [totalAccount, settotalAccountCount] = useState([]);
  const [nameList, setNamelist] = useState([]);
  const [Actname, setaccountname] = useState("");
  const [maketransaction, setTransaction] = useState(false);

  // const [mainWallet, setMainAccount] = useState({
  //   name: "Account1",
  //   wallet: null,
  //   privateKey: " ",
  //   id: null,
  // });
  const navigate = useNavigate();
  const secretkey = process.env.SECRET_KEY;
  function destroyData() {
    localStorage.clear();
    setlogout(true);
    setlogin(false);
    navigate("/");
  }
  console.log(provider);
  useEffect(() => {
    const phrase = localStorage.getItem("phrase");
    if (phrase != null) {
      setlogout(false);
      setlogin(true);
      if (phrase != null) {
        try {
          const network = localStorage.getItem("network");
          var bytes = CryptoJS.AES.decrypt(phrase, "123 my secret key");
          const decrypted = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          //if this is first time add wallet number 0
          //if that exist find seleted on and display that wallet
          //on click after create account give it number and add into list of accounts
          //after click on it add selected value of wallet and display this wallet
          const checkSelectedWallet = localStorage.getItem("SelectedWallet");
          if (checkSelectedWallet != null) {
            const wallet = createWalletfromMnemonic(
              decrypted.mnemonic,
              checkSelectedWallet
            );
            setphrases(decrypted.mnemonic);
            createProvide(network, wallet.address);
            setPrivatekey(wallet.privateKey);
            setaddress(wallet.address);
            console.log(wallet.address);
          } else {
            const wallet = createWalletfromMnemonic(
              decrypted.mnemonic,
              checkSelectedWallet
            );
            setphrases(decrypted.mnemonic);
            createProvide(network, wallet.address);
            setPrivatekey(wallet.privateKey);
            setaddress(wallet.address);
            console.log(wallet.address);
          }
          console.log("am rendring ");
        } catch (error) {
          console.log(error);
        }
      }
    } else {
      destroyData();
    }
    //decrypting
  }, []);
  useEffect(() => {
    const nameListcheck = localStorage.getItem("nameList");
    if (nameListcheck != null) {
      setNamelist([JSON.parse(nameListcheck)]);
      let list = JSON.parse(nameListcheck);
      const checkSelectedWallet = localStorage.getItem("SelectedWallet");
      setaccountname(list[checkSelectedWallet]);
    }
  }, []);

  useEffect(() => {
    const checkSelectedWallet = localStorage.getItem("SelectedWallet");
    const phrase = localStorage.getItem("phrase");
    const network = localStorage.getItem("network");

    if (phrase != null) {
      var bytes = CryptoJS.AES.decrypt(phrase, "123 my secret key");
      const decrypted = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      setphrases(decrypted.mnemonic);
      const wallet = createWalletfromMnemonic(
        decrypted.mnemonic,
        checkSelectedWallet
      );
      createProvide(network, wallet.address);
      setPrivatekey(wallet.privateKey);
      setaddress(wallet.address);
    } else {
      destroyData();
      navigate("/");
    }
    console.log("am rendring from click");
    const nameListcheck = localStorage.getItem("nameList");
    if (nameListcheck != null) {
      setNamelist([JSON.parse(nameListcheck)]);
      let list = JSON.parse(nameListcheck);
      const checkSelectedWallet = localStorage.getItem("SelectedWallet");
      setaccountname(list[checkSelectedWallet]);
    }
  }, [rendered]);

  useEffect(() => {
    const phrase = localStorage.getItem("phrase");
    if (phrase === null) {
      navigate("/");
    }
  }, [empty]);

  //create wallet from mnemonics
  function createWalletfromMnemonic(mnemonic, SelectedWallet) {
    console.log();
    const wallet = ethers.Wallet.fromMnemonic(
      mnemonic,
      `m/44'/60'/0'/0/${Number(SelectedWallet)}`
    );
    return wallet;
  }

  //creating provider
  function createProvide(network, address) {
    const provider = new ethers.providers.JsonRpcProvider(
      `https://${network}.infura.io/v3/acea3545f5964326a80fd1f10574ade0`
    );
    setprovider(provider);
    provider.getNetwork().then((res) => setShowNetwork(res.name));
    addressofwallet(provider, address);
  }
  async function addressofwallet(provider, address) {
    let Balance = await provider.getBalance(address);
    setETH(ethers.utils.formatEther(Balance));
  }
  function createAccount() {
    let namelistArray = localStorage.getItem("nameList");
    setNamelist([JSON.parse(namelistArray)]);
    setNamelist([...nameList, newAccountName]);
    localStorage.setItem("nameList", JSON.stringify(nameList));
    const wallet = ethers.Wallet.fromMnemonic(
      myphrase,
      `m/44'/60'/0'/0/${nameList.length}`
    );
    setcreated(!accountCreated);

    // settotalAccountCount(...totalAccount, wallet);
  }
  console.log(totalAccount.length);

  return (
    <div className="dashboard">
      <div className="header">
        <div className="child">
          <p className="title-text">{Actname}</p>
          <div className="second-container">
            <p className="address-holder">
              {WalletAddress.slice(0, 5)}...
              {WalletAddress.slice(-5)}
            </p>
            <MdContentCopy className="copy-icon" />
          </div>
        </div>
        <BsThreeDotsVertical
          className="side-icon"
          onClick={() => {
            setside(!sideIcon);
            console.log("clicked");
          }}
        />
        {sideIcon && (
          <div className="card">
            <div className="div1">
              <RiExternalLinkLine className="side-icon-a" />
              <span>View Account on Etherscan</span>
            </div>
            <div className="div1">
              <TbApps className="side-icon-a" />
              <span>Account details</span>
            </div>
            <div className="div1" onClick={() => setshowkey(true)}>
              <BsKey className="side-icon-a" />
              <span>privateKey</span>
            </div>
            <div className="div1" onClick={destroyData}>
              <IoLogOutOutline className="side-icon-a" />
              <span>Logout</span>
            </div>
          </div>
        )}
      </div>
      <div className="main-holder-b">
        <p>
          <FaEthereum className="main-icon" />
        </p>
        <p>{totalETH} ETH</p>
        <p>Network:{showNetwork}</p>
        <BsArrowUpRight
          className="send-ETH-icon"
          onClick={() => {
            setTransaction(true);
          }}
        />
        <p>Send</p>
        {maketransaction && (
          <TransactionCard
            setTransaction={setTransaction}
            walletPrivateKey={walletPrivateKey}
            provider={provider}
          />
        )}
        {/*this will show when i have to send some transactions*/}
        {/* <p>0.00 USD</p> */}
        {showPrivateKey && (
          <div className="privateKey">
            <div>
              <strong>chain Wallet</strong>
              <GrFormClose
                onClick={() => setshowkey(false)}
                className="close-card"
              />
            </div>
            <em>Private-Key (do not share this key with anyone )</em>
            <p>{walletPrivateKey}</p>
            <MdContentCopy className="copy-icon" />
          </div>
        )}
        {ShowCreateAccountCard && (
          <div className="create-account">
            <input
              placeholder="Acount name"
              type={"text"}
              onChange={(e) => {
                setname(e.target.value);
              }}
            />
            <button onClick={() => setcreateAccountCard(false)}>Cancel</button>
            <button onClick={createAccount}>Create</button>
          </div>
        )}
      </div>
      <div className="container">
        <div>Assets</div>
        <div>Activity</div>
      </div>
    </div>
  );
}

export default Dashboard;
