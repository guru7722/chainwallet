//if input words are lessthen 12 print 12 input feild
//if input words are greater then 12 and lessthen 15 then print 15 input feilds
//so on if any input is empty say to fill it up
//check if phrase are valid or not
//then as for password
//then encrypt wallet send him to dashboard but if those phrase do not exist then do not let them go on dashboard

//first task to create dynamic input fields
//default will be 12
// on the basis of selected value feild may change
//pasted string of words should automatic filled in space
//if words are more then it should shift to right number of feilds automatically

import React, { useEffect, useState } from "react";
import "../Style/Importwallet.css";
import { FcInfo } from "react-icons/fc";
import { FaRegEyeSlash } from "react-icons/fa";
import { RiEye2Line } from "react-icons/ri";
import { ethers } from "ethers";
import CryptoJS from "crypto-js";
import { useNavigate } from "react-router-dom";
function Importwallet() {
  const [selectVAl, setselectVAL] = useState({ val: 12 }); //Default input field size then changed based on the selected value by user
  const [arr, setarr] = useState([...new Array(selectVAl.val)].map(() => 0)); //creating an array for maping val
  const [phraseOBJ, setPhraseWords] = useState(new Map()); //it will store map object
  const [isLinepasted, setline] = useState(false);
  const [wholeStringofphrase, setstring] = useState("");
  const [isError, setError] = useState(false); //if phrase are wrong they will throw error
  const [encrypted, setencrypted] = useState(false); //checking if wallet is encrypted or not
  const [passwordSeted, setpassword] = useState(false); //checking password is seted or not
  const [mypassword, setmypass] = useState(""); //real password
  const [wait, setwait] = useState(false);
  const [isclicked, setclicked] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {
    setarr([...new Array(selectVAl.val)].map(() => 0));
  }, [selectVAl]); //rendring based on selectVAl state changed

  useEffect(() => {
    if (isLinepasted != true) {
      const arr = [];
      createAndSetmap(arr);
      setline(true);
    }
    localStorage.setItem("SelectedWallet", 0);
    localStorage.setItem("nameList", JSON.stringify(["account0"])); //default account name
    localStorage.setItem("network", "rinkeby"); //default network
  }, []);
  useEffect(() => {
    if (wait) {
      navigate("/dashboard");
      console.log("chek");
    }
  }, [wait]);

  useEffect(() => {
    if (encrypted) {
      //check if it is stored in local storage or not
      const phrase = localStorage.getItem("phrase");
      if (phrase != null) {
        setwait(true);
        window.location = "/dashboard";
        console.log("created");
      }
    }
  }, [encrypted]);

  //checking phrase are right or wrong
  function checkifphraseAreRight(mnemonics) {
    try {
      setError(false);
      if (passwordSeted) {
        encryptphraseAndStroeItinLocalStorage(wholeStringofphrase);
        const encryptPasswordAndstorItinLocalStorage = CryptoJS.AES.encrypt(
          mypassword,
          "123 my secret key"
        );
        //storing it in local storage
        localStorage.setItem("pass", encryptPasswordAndstorItinLocalStorage);
        setwait(true);
      } else {
        console.log("set password ");
      }
    } catch {
      setError(true);
      console.log("mnemonics are invalid");
    }
  }
  //On line pasted
  function HandlePaste(e, text) {
    checkifphraseAreRight(text);
    const PhraseArr = text.split(" ");
    console.log(PhraseArr.length, PhraseArr, "check");
    PhraseArr.map((val, i) => {
      if (val == "") {
        console.log("spaces");
        PhraseArr.splice(i, 1);
      }
      setstring(text);
    });
    ChangeInputfeildBasedonlengthofstring(PhraseArr);
    if (arr.length >= 2) {
      createAndSetmap(PhraseArr);
      setline(true);
    }
  }
  function encryptphraseAndStroeItinLocalStorage(mnemonic) {
    const encrytpingPhrase = CryptoJS.AES.encrypt(
      JSON.stringify({ mnemonic }),
      "123 my secret key"
    );
    localStorage.setItem("phrase", encrytpingPhrase);
  }

  // this function will work if this is a only word we are working with
  function HandleWord(e, index) {
    e.preventDefault();
    const { value } = e.target;
    const arr = value.split(" ");
    if (isLinepasted && arr.length == 1) {
      setPhraseWords(
        (map) =>
          new Map(
            map.set("index" + index, { id: index, value: value, ishide: true })
          )
      );
    } else if (isLinepasted == false) {
      if (arr.length == 1) {
        createAndSetmap(arr);
        setline(true);
      }
    }
  }

  //change input feild count based on the number of phrase
  function ChangeInputfeildBasedonlengthofstring(arr) {
    if (arr.length > 12 && arr.length <= 15) {
      setselectVAL({ val: 15 });
    } else if (arr.length > 15 && arr.length <= 18) {
      setselectVAL({ val: 18 });
    } else if (arr.length > 18 && arr.length <= 24) {
      setselectVAL({ val: 24 });
    } else if (arr.length > 24) {
      console.log("this many lengths are not allowed");
    } else {
      setselectVAL({ val: 12 });
    }
  }

  // this f(n) will create new map() object and set some values and set it to the state called phraseOBJ
  function createAndSetmap(arr) {
    let map = new Map();
    for (var i = 0; i < 24; i++) {
      if (i < arr.length) {
        map.set("index" + i, { id: i, value: arr[i], ishide: true });
      } else {
        map.set("index" + i, "");
      }
    }
    setPhraseWords(map);
  }

  //this value will be set to the input feild base on the condition is true or false
  function inputvalset(Linepasted, index) {
    if (Linepasted) {
      return phraseOBJ.get("index" + index).value;
    } else if (Linepasted != true) {
      return "";
    }
  }

  //when import wallet button is not clicked
  function ImportWallet() {
    checkifphraseAreRight(wholeStringofphrase);
    if (passwordSeted && isError == false) {
      setclicked(true);
    }
  }
  console.log(typeof wholeStringofphrase);
  return (
    <div className="main-holder">
      <h1>Import a Wallet with Secret Recovery Phrase</h1>
      <p className="p1">
        Only the first account on this wallet will auto load.After completing
        this process,to add additional accounts,click the drop down menu,then
        select Create Account.
      </p>
      <div className="select-holder">
        <h3>Secret Recovery phrase</h3>
        <select
          className="selectMe"
          onChange={(e) => {
            const selevVal = Number(e.target.value);
            setselectVAL({ val: selevVal });
          }}
        >
          <option value={12}>I have a 12 - word phrase</option>
          <option value={15}>I have a 15- word phrase</option>
          <option value={18}>I have a 18 - word phrase</option>
          <option value={24}>I have a 24 - word phrase</option>
        </select>
      </div>
      <p className="p2">
        <FcInfo />
        {isLinepasted ? <></> : <>not pasted</>}
        You can paste your entire secret recovery phrase into any field
      </p>
      <div className="phrase-input">
        {arr.map((val, index) => {
          var ishide = false;
          var typetext = "text"; //it will set value "text" or "password" based on the condition
          var vals = inputvalset(isLinepasted, index); //this is a value for every feild  use to fill input feild automatically
          console.log(index);
          return (
            <div className="child" key={index}>
              <p className="indexNumber"> {index + 1}.</p>
              <input
                type={typetext}
                value={vals}
                onPaste={(e) => HandlePaste(e, e.clipboardData.getData("Text"))}
                onChange={(e) => HandleWord(e, index)}
              />
              <button
                onClick={() => {
                  setPhraseWords(
                    (map) =>
                      new Map(
                        map.set("index" + index, {
                          id: index,
                          ishide: !false,
                        })
                      )
                  );
                }}
              >
                {true ? (
                  <FaRegEyeSlash className="icon" />
                ) : (
                  <RiEye2Line className="icon" />
                )}
              </button>
            </div>
          );
        })}
      </div>
      {isError && <p className="error-check">Invalid phrase</p>}
      {isclicked ? (
        wait ? (
          <p className="p-text">Done!</p>
        ) : (
          <p className="p-text">wait encrypting wallet...</p>
        )
      ) : (
        <></>
      )}
      <div className="password-holder">
        <label htmlFor="newpass">New password</label>
        <input
          type={"password"}
          id="newpass"
          name="newpass"
          onChange={(e) => {
            setmypass(e.target.value);
          }}
        />
        <label htmlFor="confrmpass">Confirm password</label>
        <input
          type={"password"}
          id="confrmpass"
          name="confrmpass"
          onChange={(e) => {
            if (e.target.value == mypassword) {
              console.log("password matched");
              setmypass(e.target.value);
              setpassword(true);
            } else {
              setpassword(false);
            }
          }}
        />
        {isError ? (
          <button>WrongPhrase</button>
        ) : (
          <button onClick={ImportWallet}>Import</button>
        )}
      </div>
    </div>
  );
}

export default Importwallet;
/*



*/
