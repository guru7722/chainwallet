import React, { useEffect, useState } from "react";
import { ethers } from "ethers";
import CryptoJS from "crypto-js";
import "../Style/Phrase.css";
import { passcontext } from "../App";
import { useNavigate } from "react-router-dom";
function Phrase() {
  const navigate = useNavigate();
  const [myphrase, setmyphrase] = useState("");
  useEffect(() => {
    const phrase = localStorage.getItem("phrase");
    if (phrase != null) {
      try {
        var bytes = CryptoJS.AES.decrypt(phrase, "123 my secret key");
        const decrypted = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        const phrases = decrypted.mnemonic;
        setmyphrase(phrases);
      } catch (error) {
        console.log(error);
      }
      console.log("not empty");
    } else {
      console.log("nothing here");
    }
    //decrypting
  }, []);

  function redirectME() {
    navigate("/login");
  }
  //download phrase in txt files
  function downloadFile() {
    const element = document.createElement("a");
    const file = new Blob([myphrase.toString()], {
      type: "text/plain",
    });
    element.href = URL.createObjectURL(file);
    element.download = "mnemonicPhrase.txt";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }

  return (
    <div className="Phrase">
      <h1>Secret Recovery Phrase</h1>
      <h3>Your Secret phrase make easy to backup or recover your account.</h3>
      <h6>
        <strong>WARNING :</strong>Never share or disclose your recover phrase
        with anyone .you can end up loosing your main account
      </h6>
      <div className="Phrase-holder" id="Phrase-holder">
        {myphrase}
      </div>
      <button className="BTN1" onClick={downloadFile}>
        Download
      </button>
      <button className="BTN2" onClick={redirectME}>
        Next
      </button>
    </div>
  );
}

export default Phrase;
