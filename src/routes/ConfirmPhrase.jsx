import React, { useEffect } from "react";
import { useState } from "react";
import "../Style/ConfirmPhrase.css";
function ConfirmPhrase() {
  const [isclicked, setclick] = useState(false);
  // if (isclicked) {
  //   console.log(isclicked);
  // }
  function confirmPhrase() {
    setclick(true);
  }
  useEffect(() => {
    const encryptedwallet = localStorage.getItem("JSONwallet");
    if (encryptedwallet != null && isclicked === true) {
      window.location = "/login";
    }
  });

  return (
    <div className="ConfirmPhrase">
      <h1>Confirm your Secret Recovery Phrase </h1>
      <p>Please select each phrase in order to make sure it is correct</p>
      <div className="phrase-holder"></div>
      <div className="txt-select">
        <p onClick={() => setclick(true)}>text1</p>
        {isclicked ? <></> : <p>text2</p>}
        <p>text3</p>
      </div>
      <button onClick={confirmPhrase}>click</button>
    </div>
  );
}

export default ConfirmPhrase;
