import React, { useEffect, useState } from "react";
import "../../Style/TranCardStyle.css";
import { ethers } from "ethers";

function TransactionCard({ setTransaction, walletPrivateKey, provider }) {
  const [SendingAddress, setSendAddress] = useState("w");
  const [isAddressValid, setvalid] = useState(true);
  const [sendingAmount, setAmount] = useState(0);

  useEffect(() => {
    console.log("isValidaddress...");
  }, [isAddressValid]);

  function isValidAddress(e) {
    let Value = e.target.value;
    setvalid(ethers.utils.isAddress(Value));
    setSendAddress(Value);
  }
  async function SendTransaction() {
    let wallet = new ethers.Wallet(walletPrivateKey, provider);
    // 5. Create tx object
    const tx = {
      to: SendingAddress,
      value: ethers.utils.parseEther(sendingAmount),
    };
    const createReceipt = await wallet.sendTransaction(tx);
    await createReceipt.wait();
    console.log(`Transaction successful with hash: ${createReceipt.hash}`);
    console.log("sending ETH");
  }

  console.log(SendingAddress);
  return (
    <div className="Send-main-card">
      <div className="div1">
        Send to<button onClick={() => setTransaction(false)}>Cancel</button>
      </div>
      <input
        type={"text"}
        onChange={(e) => {
          isValidAddress(e);
        }}
        placeholder="address 0x0f"
      />
      {isAddressValid ? (
        <>
          <input type="number" onChange={(e) => setAmount(e.target.value)} />
          <div className="div2">
            <button onClick={() => setTransaction(false)} className="btns">
              Cancel
            </button>
            <button onClick={SendTransaction} className="btns">
              Send
            </button>
          </div>
        </>
      ) : (
        SendingAddress.length > 0 && <p>Recipient address is invalid</p>
      )}
    </div>
  );
}

export default TransactionCard;
