import React from "react";
import "../cards/Cardstyle.css";
import { Link } from "react-router-dom";
import { useState } from "react";
import { FaDownload } from "react-icons/fa";
import { BsPlusLg } from "react-icons/bs";
function Cards(props) {
  const [Download, setDownload] = useState(props.Download); //based on this state the icon will be changed
  return (
    <div className="Card">
      {Download ? (
        <FaDownload className="icon" />
      ) : (
        <BsPlusLg className="icon" />
      )}
      <span>{props.lineOne}</span>
      <p>{props.lineTwo}</p>
      {Download ? (
        <Link to="/importwallet" className="card-link">
          {props.BTNtxt}
        </Link>
      ) : (
        <Link to="/createwallet" className="card-link">
          {props.BTNtxt}
        </Link>
      )}
    </div>
  ); //if Download is true show this icon if false then show that etc
}

export default Cards;
