import React, { useState } from "react";
import "../navbar/Nav.css";
import { SiBlockchaindotcom } from "react-icons/si";
import Profile from "../Profile";
function Navbar({
  login,
  rendered,
  setrender,
  setcreateAccountCard,
  accountCreated,
}) {
  return (
    <div className="nav">
      <strong>
        <SiBlockchaindotcom />
        ChainWallet
      </strong>
      <div>
        {login && (
          <Profile
            rendered={rendered}
            setrender={setrender}
            setcreateAccountCard={setcreateAccountCard}
            accountCreated={accountCreated}
          />
        )}
      </div>
    </div>
  );
}

export default Navbar;
