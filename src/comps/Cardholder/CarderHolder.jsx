import React from "react";
import "../Cardholder/Cardholderstyle.css";
import Cards from "../cards/Cards";

function CarderHolder() {
  return (
    <div className="Card-holder">
      <Cards
        Download={true}
        lineOne={"No,I already have Secret Recovery Phrase"}
        lineTwo={"Import your existing wallet using a Secret Recovery Phrase"}
        BTNtxt={"ImportWallet"}
      />
      <Cards
        Download={false}
        lineOne={"Yes,let's get set up!"}
        lineTwo={"This will create new wallet and new Secret Recovery Phrase"}
        BTNtxt={"CreateWallet"}
      />
    </div>
  );
}

export default CarderHolder;
