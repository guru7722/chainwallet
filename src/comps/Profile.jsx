import React, { useEffect } from "react";
import { useState } from "react";
import "../Style/profile.css";

function Profile({
  rendered,
  setrender,
  setcreateAccountCard,
  accountCreated,
}) {
  const [create, setcreate] = useState(false);
  const [network, setnetwork] = useState(false);
  const [selectedNetwork, setval] = useState("");
  const [accountlist, setlist] = useState([]);

  //useContext data from profile -> dashboard
  // useEffect(() => {
  //   localStorage.setItem("network", selectedNetwork);
  // });
  useEffect(() => {
    const network = localStorage.getItem("network");
    const namelistArray = localStorage.getItem("nameList");
    setlist([JSON.parse(namelistArray)[0]]);
    console.log();
    setval(network);
  }, []);
  useEffect(() => {
    localStorage.setItem("network", selectedNetwork);
    setrender(!rendered);
  }, [selectedNetwork]);
  console.log(accountlist[0], "chek");
  useEffect(() => {
    const namelistArray = localStorage.getItem("nameList");
    console.log(JSON.parse(namelistArray));
    setlist([JSON.parse(namelistArray)[0]]);
  }, [accountCreated]);
  return (
    <div className="my-profile pos">
      <div
        className="myDiv "
        onClick={() => {
          setnetwork(!network);
          setcreate(false);
        }}
      >
        network select
      </div>
      <div
        className="myDiv "
        onClick={() => {
          setcreate(!create);
          setnetwork(false);
        }}
      >
        Create Account
      </div>
      {network && (
        <div className="pos">
          <p
            className="posA"
            onClick={() => {
              setval("ropsten");
              setnetwork(false);
            }}
          >
            ropsten
          </p>
          <p
            className="posA"
            onClick={() => {
              setval("kovan");
              setnetwork(false);
            }}
          >
            kovan
          </p>
          <p
            className="posA"
            onClick={() => {
              setval("goerli");
              setnetwork(false);
            }}
          >
            goerli
          </p>
          <p
            className="posA"
            onClick={() => {
              setval("rinkeby");
              setnetwork(false);
            }}
          >
            rinkeby
          </p>
        </div>
      )}
      {create && (
        <div className="pos">
          {accountlist[0].map((val, index) => {
            return (
              <p
                key={index}
                className="account"
                onClick={() => {
                  console.log(index, "am from her");
                  localStorage.setItem("SelectedWallet", index);
                  setcreate(false);
                  setrender(!rendered);
                }}
              >
                {val}
              </p>
            );
          })}
          <hr></hr>
          <div
            onClick={() => {
              setcreateAccountCard(true);
              setcreate(false);
            }}
          >
            Create Account
          </div>
        </div>
      )}
    </div>
  );
}

export default Profile;
