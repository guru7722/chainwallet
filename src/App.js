import Navbar from "./comps/navbar/Navbar";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import CreatePassword from "./routes/CreatePassword";
import Phrase from "./routes/Phrase";
import ConfirmPhrase from "./routes/ConfirmPhrase";
import Home from "./routes/Home";
import Dashboard from "./routes/Dashboard";
import Importwallet from "./routes/Importwallet";
import { useEffect, useState, createContext } from "react";
import { MdLogin } from "react-icons/md";
import Login from "./routes/Login";
export const passcontext = createContext();

function App() {
  const [passdata, setphrase] = useState(""); //using usecontext for sharing data betweein same level components (createpassword -> phrase (mnemonics))
  const [password, setpass] = useState("");
  const [login, setlogin] = useState(false);
  const [islogut, setlogout] = useState(false);
  const [wallet, setwallet] = useState("");
  const [rendered, setrender] = useState(false); //this will render page when network will be changed
  const [ShowCreateAccountCard, setcreateAccountCard] = useState(false);
  const [accountCreated, setcreated] = useState(false);
  // const [networkVal, setval] = useState("");

  function showProfile() {
    setlogin(true);
  }

  useEffect(() => {
    console.log("...");
  }, [passdata]);

  // createpassword -> login (wallet)
  function walletSet(mywallet) {
    setwallet(mywallet);
  }

  //createPassword -> login (password)
  function pass(mypass) {
    setpass(mypass);
  }

  return (
    <div className="App">
      <Navbar
        login={login}
        rendered={rendered}
        setrender={setrender}
        setcreateAccountCard={setcreateAccountCard}
        accountCreated={accountCreated}
      />
      <passcontext.Provider
        value={{
          logout: [islogut, setlogout],
        }}
      >
        <Routes>
          <Route exact path="/" element={<Home />}></Route>
          <Route exact path="importwallet" element={<Importwallet />}></Route>
          <Route exact path="confirmphrase" element={<ConfirmPhrase />}></Route>
          <Route
            exact
            path="createwallet"
            element={<CreatePassword apass={pass} />}
          ></Route>
          <Route exact path="phrase" element={<Phrase />}></Route>
          <Route
            exact
            path="dashboard"
            element={
              <Dashboard
                setlogin={setlogin}
                rendered={rendered}
                ShowCreateAccountCard={ShowCreateAccountCard}
                setcreateAccountCard={setcreateAccountCard}
                setcreated={setcreated}
                accountCreated={accountCreated}
              />
            }
          ></Route>
          <Route
            exact
            path="login"
            element={<Login passfor={password} />}
          ></Route>
        </Routes>
      </passcontext.Provider>
    </div>
  );
}

export default App;
